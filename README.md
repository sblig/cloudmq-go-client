#cloudmq-go-client    
       
------------------------------------------------------------------------
增加部分新特性           
1、增加消息msgId字段解析    
2、增加消息tag和消息key字段解析     
3、增加消息storeHost、bornHost、bornTimestamp、storeTimestamp字段解析     
4、消费消息主业务，增加消费成功(CommitMessage)、消息失败(ReconsumeLater) 等业务标识              
5、消费消息，诸如topic、tag、consumerGroupId、namesrv、clientIp等参数采用配置文件读取              
6、调整consumser实例的InstanceName名称，增加进程pid标识        
7、调整原始的日志框架为beego/logs，日志级别默认Info                
8、新增tps测试用例              
          
       
---------------------------------------------------------------------------
部分未完成功能                
1、consumer业务没有shutdown功能，使用kill -9关闭程序，可能存在小部分数据丢失的情况，具体会丢失多少数据量待详细测试    
2、tps用例测试不够完善，进一步可引进其官方自带的benchmark测试，待后续完善      
3、部署到服务器后，如果配置文件cfg.json不能识别，则检查代码      
4、绝对不要在服务器配置环境变量“CORE_CONFIG”，该环境变量多个模块会读取，配置后反而导致其他模块异常       
         
---------------------------------------------------------------------------
消费业务使用示例         
```go
package main

import (
	"fmt"

	"git.oschina.net/cloudzone/cloudmq-go-client/cloudmq"
)

func main() {
	conf := &cloudmq.Config{
		Nameserver:   "10.122.1.201:9876",
		InstanceName: "DEFAULT",
	}
	consumer, err := cloudmq.NewDefaultConsumer("ConsumerGroupName", conf)
	if err != nil {
		panic(err)
	}
	consumer.Subscribe("testTopic", "*")

	var count int
	consumer.RegisterMessageListener(func(msgs []*cloudmq.MessageExt) (int, error) {
		for _, msg := range msgs {
			count++
			fmt.Printf("count=%d|msgId=%s|topic=%s|storeTimestamp=%d|bornTimestamp=%d|storeHost=%s|bornHost=%s"+
				"|msgTag=%s|msgKey=%s|sysFlag=%d|storeSize=%d|queueId=%d|body=%s\n",
				count, msg.MsgId, msg.Topic, msg.StoreTimestamp, msg.BornTimestamp, msg.StoreHost, msg.BornHost,
				msg.Tag(), msg.Key(), msg.SysFlag, msg.StoreSize, msg.QueueId, msg.Body)
		}
		return cloudmq.Action.CommitMessage, nil
	})
	consumer.Start()

	select {}
}
```

