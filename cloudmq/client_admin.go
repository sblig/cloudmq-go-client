package cloudmq

import (
	"fmt"
)

// 单独作为客户端，mqClient未初始化nameserver值，与nameserver相互独立，主要用于网络请求
// Add by chenjunjie<youaijj@qq.com>, since 2017/11/22
func NewClientAdmin(addr string) (Admin, error) {
	if addr == "" {
		return nil, fmt.Errorf("addr is null")
	}

	remotingClient := NewDefaultRemotingClient()
	mqClient := NewMqClient()
	mqClient.remotingClient = remotingClient
	mqClient.conf = &Config{ClientIp: addr}
	//mqClient.conf.ClientIp = addr
	mqClient.clientId = getMqClientClientId(mqClient.conf)

	admin := &DefaultAdmin{
		conf:           mqClient.conf,
		remotingClient: remotingClient,
		mqClient:       mqClient,
	}

	return admin, nil
}
