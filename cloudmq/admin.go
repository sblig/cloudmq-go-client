package cloudmq

import (
	"errors"
	"fmt"
	"github.com/pquerna/ffjson/ffjson"
	"sort"
	"strconv"
	"strings"
)

type Admin interface {
	SearchTopic() (*TopicData, error)
	SearchClusterInfo() (*ClusterInfo, error)
	CreateTopic(topicName, clusterName string, readQueueNum int, writeQueueNum int, order bool) (bool, error)
	MessageTrackDetail(msg *MessageExt) ([]*MessageTrack, error)
	QueryConsumerGroupByTopic(topic string) ([]string, error)
	QueryConsumerProgress(consumeGroupId string) (*ConsumerProgress, error)
	QueryConsumerConnection(consumeGroupId string) (*ConsumerConnection, error)
	FetchBrokerRuntimeStats(brokerAddr string) (*BrokerRuntimeInfo, error)
	QueryRetryGroupTopicPrefix() string
	DecodeMessageId(msgId string) (string, uint64, error)
	Close() error
	ToString() string
	//add by chenjunjie
	GetTopicRouteInfoByTopic(topic string) (*TopicRouteData, error)
	GetTopicStatsInfo(addr, topic string) ([]*TopicStore, error)
	GetKVConfig(namespace, key string) (string, error)
	QueryMsgByKey(topic, key string, maxNum int32, begin, end int64) (*QueryResult, error)
	QueryMessageById(msgId string) (*MessageExt, error)
	QueryMsgByOffset(topic, brokerName string, queueId int32, offset int64) (*MessageExt, error)
	WipeWritePermOfBroker(namesrvAddr, brokerName string) (int, error)
	GetBrokerRuntimeStats(brokerAddr string) (*kvTable, error)

	UpdateKvConfig(namespace, key, value string) (bool, error)
	UpdateBrokerConfig(brokerAddr, clusterName, key, value string) (bool, error)
	DeleteKvConfig(namespace, key string) (bool, error)
	GetProjectGroup(ip, project string) (string, error)
	UpdateProjectGroup(ip, project string) (bool, error)
	DeleteProjectGroup(ip, project string) (bool, error)
	UpdateSubGroup(subscriptionGroupConfig *SubscriptionGroupConfig, brokerAddr, clusterName string) (bool, error)
	DeleteSubscriptionGroup(groupName, brokerAddr, clusterName string) (bool, error)
	GetAllSubGroupConfig(brokerAddr string) ([]*SubscriptionGroup, error)
	GetBrokerConfig(brokerAddr string) (string, error)
	GetKvList(namespace string) (*kvTable, error)

	SetAdminConfigNamesrv(namesrv string)
}

func (admin *DefaultAdmin) SetAdminConfigNamesrv(namesrv string) {
	admin.conf.Nameserver = namesrv
}

// GetKvList 根据namespace获取namesrv配置信息的list
func (admin *DefaultAdmin) GetKvList(namespace string) (*kvTable, error) {
	table, err := admin.mqClient.getKvList(admin.conf.Nameserver, namespace, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}
	return table, err
}

// GetAllSubGroupConfig 获取所有订阅组信息
func (admin *DefaultAdmin) GetAllSubGroupConfig(brokerAddr string) (sub []*SubscriptionGroup, err error) {
	sub, err = admin.mqClient.getAllSubGroupConfig(brokerAddr, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}
	return sub, err
}

// GetBrokerConfig 获取broker配置信息
func (admin *DefaultAdmin) GetBrokerConfig(brokerAddr string) (string, error) {
	allBrokerConfig, err := admin.mqClient.getAllBrokerConfig(brokerAddr, defaultTimeoutMillis)
	if err != nil {
		return "", fmt.Errorf("can not get broker config, err:%s", err)
	}
	return allBrokerConfig, err
}

func (admin *DefaultAdmin) WipeWritePermOfBroker(namesrvAddr, brokerName string) (int, error) {
	wipeTopicCount, err := admin.mqClient.WipeWritePermOfBroker(namesrvAddr, brokerName, defaultTimeoutMillis)
	if err != nil {
		return 0, err
	}
	logger.Info("wipe write perm of broker[%s] in name server[%s] OK, %d\n", brokerName, namesrvAddr, wipeTopicCount)

	return wipeTopicCount, nil
}

// DeleteSubscriptionGroup 删除订阅组接口
func (admin *DefaultAdmin) DeleteSubscriptionGroup(groupName, brokerAddr, clusterName string) (bool, error) {
	if brokerAddr != "" {
		has, err := admin.mqClient.deleteSubscriptionGroup(brokerAddr, groupName, defaultTimeoutMillis)
		if err != nil {
			return has, err
		}
		return has, nil
	}

	if clusterName != "" {
		clusterInfo, err := admin.SearchClusterInfo()
		if err != nil || clusterInfo == nil {
			return false, err
		}
		masterAddrs, err := admin.mqClient.fetchMasterAddrByClusterName(clusterName, clusterInfo)
		if err != nil {
			return false, err
		}
		if len(masterAddrs) != 0 && masterAddrs != nil {
			for _, masterAddr := range masterAddrs {
				result, err := admin.mqClient.deleteSubscriptionGroup(masterAddr, groupName, defaultTimeoutMillis)
				if err != nil || !result {
					return false, err
				}
			}
			return true, nil
		}
	}

	return false, nil
}

// UpdateSubGroup 更新订阅组信息接口
func (admin *DefaultAdmin) UpdateSubGroup(subscriptionGroupConfigs *SubscriptionGroupConfig, brokerAddr, clusterName string) (bool, error) {
	subGroupConfig := &SubscriptionGroupConfig{
		GroupName:                    "",
		ConsumeEnable:                true,
		ConsumeFromMinEnable:         false,
		ConsumeBroadcastEnable:       false,
		RetryQueueNums:               1,
		RetryMaxTimes:                16,
		BrokerId:                     0,
		WhichBrokerWhenConsumeSlowly: 1,
	}
	subGroupConfig.GroupName = subscriptionGroupConfigs.GroupName

	subGroupConfig.ConsumeEnable = subscriptionGroupConfigs.ConsumeEnable

	subGroupConfig.ConsumeFromMinEnable = subscriptionGroupConfigs.ConsumeFromMinEnable

	subGroupConfig.ConsumeBroadcastEnable = subscriptionGroupConfigs.ConsumeBroadcastEnable

	if subscriptionGroupConfigs.RetryQueueNums != 0 {
		subGroupConfig.RetryQueueNums = subscriptionGroupConfigs.RetryQueueNums
	}
	if subscriptionGroupConfigs.RetryMaxTimes != 0 {
		subGroupConfig.RetryMaxTimes = subscriptionGroupConfigs.RetryMaxTimes
	}
	if subscriptionGroupConfigs.BrokerId != 0 {
		subGroupConfig.BrokerId = subscriptionGroupConfigs.BrokerId
	}
	if subscriptionGroupConfigs.WhichBrokerWhenConsumeSlowly != 0 {
		subGroupConfig.WhichBrokerWhenConsumeSlowly = subscriptionGroupConfigs.WhichBrokerWhenConsumeSlowly
	}
	if brokerAddr != "" {
		has, err := admin.mqClient.createSubscriptionGroup(brokerAddr, subGroupConfig, defaultTimeoutMillis)
		if err != nil {
			return false, err
		}
		return has, nil
	}
	if clusterName != "" {
		clusterInfo, err := admin.SearchClusterInfo()
		if err != nil || clusterInfo == nil {
			return false, err
		}
		masterAddrs, err := admin.mqClient.fetchMasterAddrByClusterName(clusterName, clusterInfo)
		if err != nil {
			return false, err
		}
		if len(masterAddrs) != 0 && masterAddrs != nil {
			for _, masterAddr := range masterAddrs {
				result, err := admin.mqClient.createSubscriptionGroup(masterAddr, subGroupConfig, defaultTimeoutMillis)
				if err != nil || !result {
					return false, err
				}
			}
			return true, nil
		}
	}

	return false, nil
}

// DeleteProjectGroup 删除项目组信息接口
func (admin *DefaultAdmin) DeleteProjectGroup(ip, project string) (bool, error) {
	if ip != "" {
		has, err := admin.mqClient.deleteKVConfigValue(admin.conf.Nameserver, "PROJECT_CONFIG", ip, defaultTimeoutMillis)
		if err != nil {
			return has, err
		}

		return has, nil
	}
	if project != "" {
		has, err := admin.mqClient.deleteIpsByProjectGroup(admin.conf.Nameserver, "PROJECT_CONFIG", project, defaultTimeoutMillis)
		if err != nil {
			return has, err
		}

		return has, nil
	}

	return false, fmt.Errorf("失败！")

}

// UpdateProjectGroup 更新项目组信息接口
func (admin *DefaultAdmin) UpdateProjectGroup(ip, project string) (bool, error) {
	has, err := admin.mqClient.createAndUpdateKvConfig(admin.conf.Nameserver, "PROJECT_CONFIG", ip, project, defaultTimeoutMillis)
	if err != nil {
		return has, err
	}

	return has, err
}

// GetProjectGroup 获取项目组信息接口
func (admin *DefaultAdmin) GetProjectGroup(ip, project string) (string, error) {
	if ip != "" {
		projectInfo, err := admin.mqClient.getProjectGroupByIp(admin.conf.Nameserver, ip, defaultTimeoutMillis)
		if projectInfo == "" || err != nil {
			return "", err
		}
		logger.Info("projectInfo:%v", projectInfo)

		return projectInfo, nil
	}

	if project != "" {
		ips, err := admin.mqClient.getIpsByProjectGroup(admin.conf.Nameserver, project, defaultTimeoutMillis)
		if ips == "" && err != nil {
			return "", err
		}
		logger.Info("ips:%v", ips)

		return ips, nil
	}

	return "", fmt.Errorf("ip和project不能同时为空！")
}

// DeleteKvConfig 删除nameServer配置信息接口
func (admin *DefaultAdmin) DeleteKvConfig(namespace, key string) (bool, error) {
	//has, err := admin.GetKVConfig(namespace, key)
	//logger.Info("has:%v", has)
	//if err != nil {
	//	return false, err
	//}
	//if has == "" {
	//	return false, fmt.Errorf("namespace:%v或者key：%v不存在,返回的value值为空。", namespace, key)
	//}
	result, err := admin.mqClient.deleteKVConfigValue(admin.conf.Nameserver, namespace, key, defaultTimeoutMillis)
	return result, err
}

// UpdateBrokerConfig 更新broker配置文件接口
func (admin *DefaultAdmin) UpdateBrokerConfig(brokerAddr, clusterName, key, value string) (bool, error) {
	if brokerAddr != "" {
		result, err := admin.mqClient.updateBrokerConfig(brokerAddr, key, value, defaultTimeoutMillis)
		if err != nil {
			return false, err
		}
		return result, nil
	}

	if clusterName != "" {
		clusterInfo, err := admin.SearchClusterInfo()
		if err != nil || clusterInfo == nil {
			return false, err
		}
		masterAddrs, err := admin.mqClient.fetchMasterAddrByClusterName(clusterName, clusterInfo)
		if err != nil {
			return false, err
		}
		if len(masterAddrs) != 0 && masterAddrs != nil {
			for _, masterAddr := range masterAddrs {
				result, err := admin.mqClient.updateBrokerConfig(masterAddr, key, value, defaultTimeoutMillis)
				if err != nil || !result {
					return false, err
				}
			}
			//result, err := admin.mqClient.updateBrokerConfig(masterAddr, key, value, defaultTimeoutMillis)
			//if err != nil {
			//	return false, err
			//}
			return true, nil
		}
	}

	return false, fmt.Errorf("更新配置文件失败！")
}

// UpdateKvConfig 更新nameServer配置信息接口
func (admin *DefaultAdmin) UpdateKvConfig(namespace, key, value string) (bool, error) {
	result, err := admin.mqClient.createAndUpdateKvConfig(admin.conf.Nameserver, namespace, key, value, defaultTimeoutMillis)
	return result, err
}

// GetKVConfig 查询nameServer配置信息接口
func (admin *DefaultAdmin) GetKVConfig(namespace, key string) (string, error) {
	result, err := admin.mqClient.getKVConfigValue(admin.conf.Nameserver, namespace, key, defaultTimeoutMillis)
	return result, err
}

// GetTopic 查询topic列表接口
func (admin *DefaultAdmin) SearchTopic() (*TopicData, error) {
	remotingCommand := createRemotingCommand(GET_ALL_TOPIC_LIST_FROM_NAMESERVER)
	response, err := admin.remotingClient.invokeSync(admin.conf.Nameserver, remotingCommand, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	if response != nil && response.Code == SUCCESS {
		topicData := new(TopicData)
		err = ffjson.Unmarshal(response.Body, topicData)
		if err != nil {
			logger.Error("%s", err)
			return nil, err
		}
		return topicData, nil
	}

	return &TopicData{}, nil
}

// GetTopicRouteInfoByTopic 获取路由信息
func (admin *DefaultAdmin) GetTopicRouteInfoByTopic(topic string) (*TopicRouteData, error) {
	topicRouteData, err := admin.mqClient.getTopicRouteInfoFromNameServer(topic, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	return topicRouteData, nil
}

// GetTopicStatsInfo 获取路由信息
func (admin *DefaultAdmin) GetTopicStatsInfo(addr, topic string) ([]*TopicStore, error) {
	result, err := admin.mqClient.getTopicStatsInfo(addr, topic, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	return result, nil
}

// SearchClusterInfo 查询集群的信息
func (admin *DefaultAdmin) SearchClusterInfo() (*ClusterInfo, error) {
	remotingCommand := createRemotingCommand(GET_BROKER_CLUSTER_INFO)
	response, err := admin.remotingClient.invokeSync(admin.conf.Nameserver, remotingCommand, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	if response != nil && response.Code == SUCCESS {
		clusterInfo := new(ClusterInfo)
		err = ffjson.Unmarshal(response.Body, clusterInfo)
		if err != nil {
			logger.Error("search clusterInfo err: %s", err.Error())
			return nil, err
		}
		return clusterInfo, nil
	}

	return &ClusterInfo{}, nil
}

// CreateTopic 创建topic
func (admin *DefaultAdmin) CreateTopic(topicName, clusterName string, readQueueNum int, writeQueueNum int, order bool) (bool, error) {
	clusterInfo, _ := admin.SearchClusterInfo()
	if clusterInfo == nil {
		return false, errors.New("CreateTopic get clusterInfo failed")
	}

	remotingCommand := createRemotingCommand(UPDATE_AND_CREATE_TOPIC)
	extFields := make(map[string]string)
	extFields["topic"] = topicName
	extFields["defaultTopic"] = "MY_DEFAULT_TOPIC"
	extFields["readQueueNums"] = strconv.Itoa(readQueueNum)
	extFields["writeQueueNums"] = strconv.Itoa(writeQueueNum)
	extFields["perm"] = "6"
	extFields["topicFilterType"] = "SINGLE_TAG"
	extFields["topicSysFlag"] = "0"
	extFields["order"] = fmt.Sprintf("%t", order)
	remotingCommand.ExtFields = extFields

	var flag bool
	for clsName, brokerNames := range clusterInfo.ClusterAddrTable {
		if clsName != clusterName {
			continue
		}
		for _, brName := range brokerNames {
			fmt.Println("brName=%s", brName)
			if brokerData, ok := clusterInfo.BrokerAddrTable[brName]; ok {
				for k, v := range brokerData.BrokerAddrs {
					if k == 0 {
						flag = true
						cmd, err := admin.remotingClient.invokeSync(v, remotingCommand, defaultTimeoutMillis)
						fmt.Println("flag=%s", flag)
						if err != nil {
							return false, err
						}
						if cmd.Code != SUCCESS {
							return false, fmt.Errorf("创建Topic失败，错误码:%d", cmd.Code)
						}
					}
				}
			}
		}
	}

	return flag, nil
}

// QueryMessageById 根据msgid查询消息详情
func (admin *DefaultAdmin) QueryMessageById(msgId string) (*MessageExt, error) {
	addr, offset, err := decodeMessageId(msgId)
	if err != nil {
		return nil, err
	}

	requestHeader := new(ViewMessageRequestHeader)
	requestHeader.Offset = offset
	remotingCommand := createRemotingCommand(VIEW_MESSAGE_BY_ID)
	remotingCommand.ExtFields = requestHeader

	response, err := admin.remotingClient.invokeSync(addr, remotingCommand, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	if response != nil && response.Code == SUCCESS {
		msgs := decodeMessage(response.Body)
		if len(msgs) > 0 {
			msg := msgs[0]
			msg.MsgId = msgId
			return msg, nil
		}
	}

	return nil, nil
}

// QueryMsgByOffset 根据offset查找msg
// add by: chenjunjie, <youaijj@163.com>
func (admin *DefaultAdmin) QueryMsgByOffset(topic, brokerName string, queueId int32, offset int64) (*MessageExt, error) {
	mq := new(MessageQueue)
	mq.Topic = topic
	mq.BrokerName = brokerName
	mq.QueueId = queueId
	consumerGroupName := TOOLS_CONSUMER_GROUP
	defaultMQPullConsumer, err := NewDefaultConsumer(consumerGroupName, admin.conf)
	if err != nil {
		return nil, fmt.Errorf(" new defaultMQPullConsumer failed.")
	}

	defaultMQPullConsumer.Start() //
	defer defaultMQPullConsumer.Shutdown()

	pullResult, err := defaultMQPullConsumer.pull(mq, "*", offset, 1) //同步
	if pullResult != nil {
		switch pullResult.PullStatus {
		case PullStatus.FOUND:
			logger.Info("PullStatus is FOUND")
			msg, err := admin.QueryMessageById(pullResult.MsgFoundList[0].MsgId) //why do query again
			if err != nil {
				return nil, err
			}
			fmt.Println(msg.toString())
			return msg, err
		case PullStatus.NO_MATCHED_MSG:
		case PullStatus.NO_NEW_MSG:
		case PullStatus.OFFSET_ILLEGAL:
		}
	}
	logger.Info("pullResult is null")
	return nil, nil

}

// QueryMsgByKey 根据key查找msg
// key: 	msg key, query msg consumerQueue form indexFile
// maxNum: 	if every query msg num > maxNume, reture maxNum Msg
// begin: 	msg store start time
// end: 	msg store expired time
// add by: 	chenjunjie, <youaijj@163.com>
func (admin *DefaultAdmin) QueryMsgByKey(topic, key string, maxNum int32, begin, end int64) (*QueryResult, error) {
	topicRouteData, err := admin.mqClient.getTopicRouteInfoFromNameServer(topic, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}
	if topicRouteData == nil {
		return nil, nil
	}

	requestHeader := &QueryMessageRequestHeader{
		Topic:          topic,
		Key:            key,
		MaxNum:         maxNum,
		BeginTimestamp: begin,
		EndTimestamp:   end,
	}

	messageList := []*MessageExt{}
	indexLastUpdateTimestamp := int64(0)
	for _, bd := range topicRouteData.BrokerDatas {
		queryResultList := []QueryResult{}
		for _, oneBrokerAddr := range bd.BrokerAddrs {
			logger.Info("oneBrokerAddr is %s", oneBrokerAddr)
			response, err := admin.mqClient.queryMessageSyncByKey(oneBrokerAddr, requestHeader, defaultTimeoutMillis)
			if err != nil {
				logger.Warn("queryMessageSyncByKey error %s", err)
				continue
			}
			if response != nil {
				switch response.Code {
				case SUCCESS:
					{
						responseCustomHeader := new(QueryMessageResponseHeader)
						map1 := ExtFields2Map(response.ExtFields)
						err := decodeCommandCustomHeader(map1, responseCustomHeader)
						if err != nil {
							return nil, err
						}

						msgs := decodeMessage(response.Body)
						qr := QueryResult{responseCustomHeader.IndexLastUpdateTimestamp, msgs}
						queryResultList = append(queryResultList, qr)
					}
				default:
					logger.Warn("getResponseCommand failed, {} {}", response.Code, response.Remark)
					break
				}
			}
		}

		for _, qr := range queryResultList {
			if qr.IndexLastUpdateTimestamp > indexLastUpdateTimestamp {
				indexLastUpdateTimestamp = qr.IndexLastUpdateTimestamp
			}

			for _, msgExt := range qr.MessageList {
				keys := msgExt.Properties[PROPERTY_KEYS]
				if keys != "" {
					matched := false
					keyArray := strings.Split(keys, KEY_SEPARATOR)
					if keyArray != nil {
						for _, k := range keyArray {
							if strings.Compare(key, k) == 0 {
								matched = true
								break
							}
						}
					}

					if matched {
						messageList = append(messageList, msgExt)
					} else {
						logger.Warn("queryMessage, find message key not matched, maybe hash duplicate {}", msgExt.toString())
					}
				}
			}
		}
	}

	if len(messageList) > 0 {
		for _, msgExt := range messageList {
			fmt.Println(msgExt.toString())
		}
		return &QueryResult{indexLastUpdateTimestamp, messageList}, nil
	} else {
		logger.Info("query operation over, but no message.")
	}

	return nil, nil
}

// MessageTrackDetail 根据msgid查询消息轨迹
func (admin *DefaultAdmin) MessageTrackDetail(msg *MessageExt) ([]*MessageTrack, error) {
	groupList, err := admin.queryTopicConsumeByWho(msg.Topic)
	if err != nil {
		return nil, err
	}

	if groupList == nil {
		return []*MessageTrack{}, nil
	}

	var tracks []*MessageTrack
	for _, group := range groupList.GroupList {
		logger.Info("#### consume group is [%s] ",group)
		track := NewMessageTrack(group)
		consumerConnection, err := admin.getConsumerConnectionList(group)
		if err != nil {
			track.ExceptionDesc = err.Error()
			tracks = append(tracks, track)
			continue
		}
		if consumerConnection == nil || len(consumerConnection.ConnectionSet) == 0 {
			track.Code = CONSUMER_NOT_ONLINE
			track.ExceptionDesc = fmt.Sprintf("CODE: %d DESC: the consumer group[%s] not online.", CONSUMER_NOT_ONLINE, group)
			tracks = append(tracks, track)
			continue
		}

		switch consumerConnection.ConsumeType {
		case ConsumeTypes.Actively:
			track.TrackType = TrackTypes.SubscribedButPull
			track.Code = SUCCESS
		case ConsumeTypes.Passively:
			flag, err := admin.consumed(msg, group)
			if err != nil {
				track.ExceptionDesc = err.Error()
				break
			}

			if flag {
				track.TrackType = TrackTypes.SubscribedAndConsumed
				track.Code = SUCCESS
				// 查看订阅关系是否匹配
				for topic, subscriptionData := range consumerConnection.SubscriptionTable {
					if topic != msg.Topic {
						continue
					}
					logger.Info("msg topic=%s, msg tag=%s ",msg.Topic, msg.Tag())
					for _, tag := range subscriptionData.TagsSet {
						logger.Info("this topic contain tag ----> %s",tag)
						if tag == msg.Tag() || tag == "*"  {
							track.TrackType = TrackTypes.SubscribedAndConsumed
							//track.Code = SUCCESS
							break
						}else {
							track.TrackType = TrackTypes.SubscribedButFilterd
							//track.Code = SUCCESS
						}
					}
				}

			} else {
				track.TrackType = TrackTypes.SubscribedAndNotConsumeYet
				track.Code = SUCCESS
			}
		default:
		}

		tracks = append(tracks, track)
	}

	return tracks, nil
}

func (admin *DefaultAdmin) consumed(msg *MessageExt, group string) (bool, error) {
	consumeStats, err := admin.getConsumeStats(group, "")
	if err != nil {
		return false, err
	}

	clusterInfo, err := admin.SearchClusterInfo()
	if err != nil {
		return false, err
	}

	for mq, ow := range consumeStats.OffsetTable {
		if mq.Topic == msg.Topic && mq.QueueId == msg.QueueId {
			if bd, ok := clusterInfo.BrokerAddrTable[mq.BrokerName]; ok {
				if addr, yes := bd.BrokerAddrs[MASTER_ID]; yes {
					if addr == msg.StoreHost && ow.ConsumerOffset > msg.QueueOffset {
						return true, nil
					}
				}
			}
		}
	}

	return false, nil
}

// 在线consumerGroup
func (admin *DefaultAdmin) queryTopicConsumeByWho(topic string) (*GroupList, error) {

	topicRouteData, err := admin.mqClient.getTopicRouteInfoFromNameServer(topic, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}
	i := 0
	for _, bd := range topicRouteData.BrokerDatas {
		fmt.Println(topicRouteData.BrokerDatas[i])
		i = i + 1
		for _, ba := range bd.BrokerAddrs {
			groupList, err := admin.mqClient.queryTopicConsumeByWho(ba, topic, defaultTimeoutMillis)
			if err != nil {
				return nil, err
			}

			return groupList, nil
		}
	}

	return &GroupList{}, nil
}

// 在线consume
func (admin *DefaultAdmin) getConsumerConnectionList(group string) (*ConsumerConnection, error) {

	topic := getRetryTopic(group)
	topicRouteData, err := admin.mqClient.getTopicRouteInfoFromNameServer(topic, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	for _, bd := range topicRouteData.BrokerDatas {
		for _, ba := range bd.BrokerAddrs {
			consumerConnection, err := admin.mqClient.getConsumerConnectionList(ba, group, defaultTimeoutMillis)
			if err != nil {
				return nil, err
			}

			return consumerConnection, nil
		}
	}

	return &ConsumerConnection{}, nil
}

// QueryConsumerGroupByTopic 根据topic查询消费组
func (admin *DefaultAdmin) QueryConsumerGroupByTopic(topic string) ([]string, error) {
	groupList, err := admin.queryTopicConsumeByWho(topic)
	if err != nil {
		return nil, err
	}

	if groupList == nil {
		return []string{}, nil
	}

	return groupList.GroupList, nil
}

// 消费进度
func (admin *DefaultAdmin) getConsumeStats(consumeGroupId, topic string) (*ConsumeStats, error) {
	retryTopic := getRetryTopic(consumeGroupId)
	topicRouteData, err := admin.mqClient.getTopicRouteInfoFromNameServer(retryTopic, defaultTimeoutMillis)
	if err != nil {
		return nil, err
	}

	result := new(ConsumeStats)
	result.OffsetTable = make(map[MessageQueue]OffsetWrapper)
	for _, bd := range topicRouteData.BrokerDatas {
		for _, ba := range bd.BrokerAddrs {
			// 由于查询时间戳会产生IO操作，可能会耗时较长，所以超时时间设置为15s
			consumeStats, err := admin.mqClient.getConsumeStats(ba, consumeGroupId, topic, defaultTimeoutMillis*5)
			if err != nil {
				return nil, err
			}

			for mq, ow := range consumeStats.OffsetTable {
				// BrokerOffset < ConsumerOffset的broke是未消费groupId的节点
				if ow.BrokerOffset < ow.ConsumerOffset {
					continue
				}
				result.OffsetTable[mq] = ow
			}

			result.ConsumeTps += consumeStats.ConsumeTps
		}
	}

	return result, nil
}

// QueryConsumerProgress 根据consumeGroupId查询消费进度
func (admin *DefaultAdmin) QueryConsumerProgress(consumeGroupId string) (*ConsumerProgress, error) {
	consumeStats, err := admin.getConsumeStats(consumeGroupId, "")
	if err != nil {
		return nil, err
	}

	length := len(consumeStats.OffsetTable)
	if length == 0 {
		return &ConsumerProgress{}, nil
	}

	mqs := make([]*MessageQueue, 0, len(consumeStats.OffsetTable))
	for mq, _ := range consumeStats.OffsetTable {
		cmq := mq.clone()
		mqs = append(mqs, cmq)
	}
	var messageQueues MessageQueues = mqs
	sort.Sort(messageQueues)

	consumerProgress := new(ConsumerProgress)
	for _, mq := range messageQueues {
		if ow, ok := consumeStats.OffsetTable[*mq]; ok {
			cg := new(ConsumerGroup)
			cg.Topic = mq.Topic
			cg.BrokerName = mq.BrokerName
			cg.BrokerOffset = ow.BrokerOffset
			cg.ConsumerOffset = ow.ConsumerOffset
			cg.QueueId = mq.QueueId
			cg.Diff = ow.BrokerOffset - ow.ConsumerOffset
			consumerProgress.Diff += cg.Diff
			consumerProgress.List = append(consumerProgress.List, cg)
		}
	}
	consumerProgress.Tps = consumeStats.ConsumeTps
	consumerProgress.GroupId = consumeGroupId

	return consumerProgress, nil
}

// QueryConsumerConnection 根据consumeGroupId查询消费进程
func (admin *DefaultAdmin) QueryConsumerConnection(consumeGroupId string) (*ConsumerConnection, error) {
	return admin.getConsumerConnectionList(consumeGroupId)
}

// FetchBrokerRuntimeStats 根据broker addr查询在线broker运行统计信息
func (admin *DefaultAdmin) FetchBrokerRuntimeStats(brokerAddr string) (*BrokerRuntimeInfo, error) {
	return admin.mqClient.getBrokerRuntimeInfo(brokerAddr, defaultTimeoutMillis)
}

// GetBrokerRuntimeStats 根据broker addr查询在线broker运行统计信息
func (admin *DefaultAdmin) GetBrokerRuntimeStats(brokerAddr string) (*kvTable, error) {
	return admin.mqClient.getBrokerRuntimeKvTable(brokerAddr, defaultTimeoutMillis)
}

// Close 关闭Admin连接
func (admin *DefaultAdmin) Close() error {
	if admin == nil || admin.mqClient == nil {
		return nil
	}
	return admin.mqClient.Close()
}

// ToString 打印DefaultAdmin基础信息
func (admin *DefaultAdmin) ToString() string {
	clientIp := admin.conf.ClientIp
	nameSrvAddr := admin.conf.Nameserver
	instanceName := admin.conf.InstanceName
	clientId := admin.mqClient.clientId

	format := "admin [clientIp=%s, nameSrvAddr=%s, instanceName=%s, clientId=%s]"
	value := fmt.Sprintf(format, clientIp, nameSrvAddr, instanceName, clientId)
	return value
}

// DecodeMessageId 解析消息ID
func (admin *DefaultAdmin) DecodeMessageId(msgId string) (storeHost string, offset uint64, err error) {
	storeHost, offset, err = decodeMessageId(msgId)
	return storeHost, offset, err
}

// QueryRetryGroupTopicPrefix 获取重试队列的前缀
func (admin *DefaultAdmin) QueryRetryGroupTopicPrefix() string {
	return retry_group_topic_prefix
}
