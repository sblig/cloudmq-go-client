package cloudmq

type GetRouteInfoRequestHeader struct {
	Topic string `json:"topic"`
}

type QueryTopicConsumeByWhoRequestHeader struct {
	Topic string `json:"topic"`
}

type GetConsumerConnectionListRequestHeader struct {
	ConsumerGroup string `json:"consumerGroup"`
}

type GetConsumeStatsRequestHeader struct {
	ConsumerGroup string `json:"consumerGroup"`
	Topic         string `json:"topic"`
}

type GetKVConfigRequestHeader struct {
	Namespace string `json:"namespace"`
	Key       string `json:"key"`
}

type GetBrokerConfigRequestHeader struct {
	BrokerAddr string `json:"brokerAddr"`
	Key        string `json:"key"`
}

type PutKVConfigRequestHeader struct {
	Namespace string `json:"namespace"`
	Key       string `json:"key"`
	Value     string `json:"value"`
}

type DeleteKVConfigRequestHeader struct {
	Namespace string `json:"namespace"`
	Key       string `json:"key"`
}

type GetKVListRequestHeader struct {
	Namespace string `json:"namespace"`
}

type GetConsumerListByGroupRequestHeader struct {
	ConsumerGroup string `json:"consumerGroup"`
}

type QueryConsumerOffsetRequestHeader struct {
	ConsumerGroup string `json:"consumerGroup"`
	Topic         string `json:"topic"`
	QueueId       int32  `json:"queueId"`
}

type ViewMessageRequestHeader struct {
	Offset uint64 `json:"offset"`
}

type AllSubGroupConfig struct {
	DataVersion            map[string]interface{}            `json:"dataVersion"`
	SubscriptionGroupTable map[string]map[string]interface{} `json:"subscriptionGroupTable"`
}

type SubscriptionGroup struct {
	SubGroupName            string                   `json:"subGroupName"`
	SubscriptionGroupConfig *SubscriptionGroupConfig `json:"subscriptionGroupConfig"`
}

type SubscriptionGroupConfig struct {
	GroupName                    string `json:"groupName"`                    // 订阅组名
	ConsumeEnable                bool   `json:"consumeEnable"`                // 消费功能是否开启
	ConsumeFromMinEnable         bool   `json:"consumeFromMinEnable"`         // 是否允许从队列最小位置开始消费，线上默认会设置为false
	ConsumeBroadcastEnable       bool   `json:"consumeBroadcastEnable"`       // 是否允许广播方式消费
	RetryQueueNums               int64  `json:"retryQueueNums"`               // 消费失败的消息放到一个重试队列，每个订阅组配置几个重试队列
	RetryMaxTimes                int64  `json:"retryMaxTimes"`                // 重试消费最大次数，超过则投递到死信队列，不再投递，并报警
	BrokerId                     int64  `json:"brokerId"`                     // 从哪个Broker开始消费
	WhichBrokerWhenConsumeSlowly int64  `json:"whichBrokerWhenConsumeSlowly"` // 发现消息堆积后，将Consumer的消费请求重定向到另外一台Slave机器
}

type DeleteSubscriptionGroupRequestHeader struct {
	GroupName string `json:"groupName"`
}

type QueryMessageRequestHeader struct {
	Topic          string `json:"topic"`
	Key            string `json:"key"`
	MaxNum         int32  `json:"maxNum"`
	BeginTimestamp int64  `json:"beginTimestamp"`
	EndTimestamp   int64  `json:"endTimestamp"`
}

type WipeWritePermOfBrokerRequestHeader struct {
	BrokerName string `json:"brokerName"`
}

type GetTopicStatsInfoRequestHeader struct {
	Topic string `json:"topic"`
}

func (t *GetTopicStatsInfoRequestHeader) checkFields() error {
	return nil
}
