package cloudmq

import (
	"fmt"
	"os"
)

const (
	defaultTimeoutMillis     = 3000 * 1000
	retry_group_topic_prefix = "%RETRY%" // 重试队列Topic名称前缀
	MASTER_ID                = 0
)

// TopicData topic data
type TopicData struct {
	List []string `json:"topicList"`
}

// ClusterInfo cluster info
type ClusterInfo struct {
	BrokerAddrTable  map[string]BrokerData `json:"brokerAddrTable"`
	ClusterAddrTable map[string][]string   `json:"clusterAddrTable"`
}

type DefaultAdmin struct {
	conf           *Config
	remotingClient RemotingClient
	mqClient       *MqClient
}

func NewDefaultAdmin(conf *Config) (Admin, error) {
	if conf.ClientIp == "" {
		conf.ClientIp = DEFAULT_IP
	}

	remotingClient := NewDefaultRemotingClient()
	mqClient := NewMqClient()
	mqClient.remotingClient = remotingClient
	mqClient.conf = conf
	mqClient.clientId = getMqClientClientId(conf)

	err := mqClient.initInfo()
	if err != nil {
		return nil, err
	}

	admin := &DefaultAdmin{
		conf:           conf,
		remotingClient: remotingClient,
		mqClient:       mqClient,
	}

	return admin, nil
}

func getRetryTopic(consumerGroup string) string {
	return retry_group_topic_prefix + consumerGroup
}

func getMqClientClientId(conf *Config) string {
	return fmt.Sprintf("%s@%d#%d#%d", conf.ClientIp, os.Getpid(), HashCode(conf.Nameserver), UnixNano())
}
