package cloudmq

var TrackTypes = struct {
	SubscribedAndConsumed      int //订阅了，而且消费了（Offset越过了）
	SubscribedButFilterd       int //订阅了，但是被过滤掉了
	SubscribedButPull          int //订阅了，但是PULL，结果未知
	SubscribedAndNotConsumeYet int //订阅了，但是没有消费（Offset小）
	UnknowExeption             int //未知异常
}{
	0,
	1,
	2,
	3,
	4,
}

type MessageTrack struct {
	ConsumerGroup string `json:"consumerGroup"` // 消费组
	TrackType     int    `json:"trackType"`     // 消息轨迹类型
	ExceptionDesc string `json:"exceptionDesc"` // 消费组描述
	Code          int    `json:"code"`          // 0:查询成功, 非0:查询异常
}

func NewMessageTrack(consumerGroup string) *MessageTrack {
	messageTrack := &MessageTrack{
		ConsumerGroup: consumerGroup,
		TrackType:     TrackTypes.UnknowExeption,
		Code:          SYSTEM_ERROR,
	}
	return messageTrack
}
