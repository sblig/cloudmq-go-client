package cloudmq

type GetKVConfigResponseHeader struct {
	Value string `json:"value"`
}

// 继承CommandCustomHeader
type QueryMessageResponseHeader struct {
	IndexLastUpdateTimestamp int64 `json:"indexLastUpdateTimestamp"`
	IndexLastUpdatePhyoffset int64 `json:"indexLastUpdatePhyoffset"`
}

type QueryResult struct {
	IndexLastUpdateTimestamp int64         `json:"indexLastUpdateTimestamp"`
	MessageList              []*MessageExt `json:"MessageExts"`
}

type PullMessageResponseHeader struct {
	SuggestWhichBrokerId int64 `json:"suggestWhichBrokerId"`
	NextBeginOffset      int64 `json:"nextBeginOffset"`
	MinOffset            int64 `json:"minOffset"`
	MaxOffset            int64 `json:"maxOffset"`
}

func (t *QueryMessageResponseHeader) CheckFields() error {
	return nil
}

type WipeWritePermOfBrokerResponseHeader struct {
	WipeTopicCount int `json:"wipeTopicCount"`
}

func (t *WipeWritePermOfBrokerResponseHeader) CheckFields() error {
	return nil
}

func (t *PullMessageResponseHeader) CheckFields() error {
	return nil
}

type TopicStoreStatus struct {
	Value []*TopicStore `json:"topicStore"`
}

type TopicStore struct {
	MessageQueue
	TopicOffset
}

type TopicOffset struct {
	MinOffset           int64 `json:"minOffset"`
	MaxOffset           int64 `json:"maxOffset"`
	LastUpdateTimestamp int64 `json:"lastUpdateTimestamp"`
}
