package cloudmq

import (
	"fmt"
	"net"
	"os"
	"strconv"
	//"sync/atomic"
	"strings"
	"time"
)

const (
	BrokerSuspendMaxTimeMillis       = 1000 * 15
	FLAG_COMMIT_OFFSET         int32 = 0x1 << 0
	FLAG_SUSPEND               int32 = 0x1 << 1
	FLAG_SUBSCRIPTION          int32 = 0x1 << 2
	FLAG_CLASS_FILTER          int32 = 0x1 << 3

	TOOLS_CONSUMER_GROUP   = "TOOLS_CONSUMER"
	DEFAULT_CONSUMER_GROUP = "DEFAULT_CONSUMER"
)

type MessageListener func(msgs []*MessageExt) (int, error)

var DEFAULT_IP = GetLocalIp4()

type Config struct {
	Nameserver   string
	ClientIp     string
	InstanceName string
}

type Consumer interface {
	//Admin
	Start() error
	Shutdown()
	RegisterMessageListener(listener MessageListener)
	Subscribe(topic string, subExpression string)
	UnSubcribe(topic string)
	SendMessageBack(msg MessageExt, delayLevel int) error
	SendMessageBack1(msg MessageExt, delayLevel int, brokerName string) error
	fetchSubscribeMessageQueues(topic string) error
	pullMessage(pullRequest *PullRequest) //启动消息拉取服务时使用，异步实现
	//add by chenjunjie
	pull(mq *MessageQueue, subExpression string, offset int64, maxNums int32) (*PullResult, error) //同步拉取消息
}

type DefaultConsumer struct {
	conf             *Config
	consumerGroup    string
	consumeFromWhere string
	consumerType     string
	messageModel     string
	unitMode         bool

	subscription    map[string]string
	messageListener MessageListener
	offsetStore     OffsetStore
	brokers         map[string]net.Conn

	rebalance      *Rebalance
	remotingClient RemotingClient
	mqClient       *MqClient
}

func NewDefaultConsumer(name string, conf *Config) (Consumer, error) {
	if conf == nil {
		conf = &Config{
			Nameserver:   os.Getenv("ROCKETMQ_NAMESVR"),
			InstanceName: "DEFAULT",
		}
	}

	if conf.ClientIp == "" {
		conf.ClientIp = DEFAULT_IP
	}

	remotingClient := NewDefaultRemotingClient()
	mqClient := NewMqClient()

	rebalance := NewRebalance()
	rebalance.groupName = name
	rebalance.mqClient = mqClient

	offsetStore := new(RemoteOffsetStore)
	offsetStore.mqClient = mqClient
	offsetStore.groupName = name
	offsetStore.offsetTable = make(map[MessageQueue]int64)

	pullMessageService := NewPullMessageService()

	consumer := &DefaultConsumer{
		conf:             conf,
		consumerGroup:    name,
		consumeFromWhere: "CONSUME_FROM_LAST_OFFSET",
		subscription:     make(map[string]string),
		offsetStore:      offsetStore,
		brokers:          make(map[string]net.Conn),
		rebalance:        rebalance,
		remotingClient:   remotingClient,
		mqClient:         mqClient,
	}

	mqClient.consumerTable[name] = consumer
	mqClient.remotingClient = remotingClient
	mqClient.conf = conf
	mqClient.clientId = fmt.Sprintf("%s@%d#%d#%d", conf.ClientIp, os.Getpid(), HashCode(conf.Nameserver), UnixNano())
	mqClient.pullMessageService = pullMessageService

	rebalance.consumer = consumer
	pullMessageService.consumer = consumer

	return consumer, nil
}

func (self *DefaultConsumer) Start() error {
	self.mqClient.start()
	return nil
}

func (self *DefaultConsumer) Shutdown() {
}

func (self *DefaultConsumer) RegisterMessageListener(messageListener MessageListener) {
	self.messageListener = messageListener
}

func (self *DefaultConsumer) Subscribe(topic string, subExpression string) {
	self.subscription[topic] = subExpression

	subData := &SubscriptionData{
		Topic:     topic,
		SubString: subExpression,
	}
	self.rebalance.subscribe(topic, subData)
}

func (self *DefaultConsumer) UnSubcribe(topic string) {
	delete(self.subscription, topic)
	self.rebalance.unSubscribe(topic)
}

func (self *DefaultConsumer) SendMessageBack(msg MessageExt, delayLevel int) error {
	return nil
}

func (self *DefaultConsumer) SendMessageBack1(msg MessageExt, delayLevel int, brokerName string) error {
	return nil
}

func (self *DefaultConsumer) fetchSubscribeMessageQueues(topic string) error {
	return nil
}

func (self *DefaultConsumer) pullMessage(pullRequest *PullRequest) {

	commitOffsetEnable := false
	commitOffsetValue := int64(0)

	commitOffsetValue = self.offsetStore.readOffset(pullRequest.messageQueue, READ_FROM_MEMORY)
	if commitOffsetValue > 0 {
		commitOffsetEnable = true
	}

	var sysFlag int32 = 0
	if commitOffsetEnable {
		sysFlag |= FLAG_COMMIT_OFFSET
	}

	sysFlag |= FLAG_SUSPEND

	subscriptionData, ok := self.rebalance.subscriptionInner[pullRequest.messageQueue.Topic]
	var subVersion int64
	var subString string
	if ok {
		subVersion = subscriptionData.SubVersion
		subString = subscriptionData.SubString

		sysFlag |= FLAG_SUBSCRIPTION
	}

	requestHeader := new(PullMessageRequestHeader)
	requestHeader.ConsumerGroup = pullRequest.consumerGroup
	requestHeader.Topic = pullRequest.messageQueue.Topic
	requestHeader.QueueId = pullRequest.messageQueue.QueueId
	requestHeader.QueueOffset = pullRequest.nextOffset

	requestHeader.SysFlag = sysFlag
	requestHeader.CommitOffset = commitOffsetValue
	requestHeader.SuspendTimeoutMillis = BrokerSuspendMaxTimeMillis

	if ok {
		requestHeader.SubVersion = subVersion
		requestHeader.Subscription = subString
	}

	pullCallback := func(responseFuture *ResponseFuture) {
		var nextBeginOffset int64 = pullRequest.nextOffset

		if responseFuture != nil {
			responseCommand := responseFuture.responseCommand
			if responseCommand.Code == SUCCESS && len(responseCommand.Body) > 0 {
				var err error
				pullResult, ok := responseCommand.ExtFields.(map[string]interface{})
				if ok {
					if nextBeginOffsetInter, ok := pullResult["nextBeginOffset"]; ok {
						if nextBeginOffsetStr, ok := nextBeginOffsetInter.(string); ok {
							nextBeginOffset, err = strconv.ParseInt(nextBeginOffsetStr, 10, 64)
							if err != nil {
								logger.Error("%s", err)
								return
							}

						}

					}

				}

				// 解析消息所有属性
				msgs := decodeMessage(responseFuture.responseCommand.Body)
				// 执行消息消费业务，返回消息被消费的结果
				result, err := self.messageListener(msgs)
				if err != nil {
					logger.Error("consume msg error: %s", err)
					//TODO retry
				} else {
					if result == Action.CommitMessage {
						// 业务处理完毕并返回CommitMessage，消费成功，更新offset
						self.offsetStore.updateOffset(pullRequest.messageQueue, nextBeginOffset, false)
					} else {
						//  业务处理完毕并返回ReconsumeLater，消费失败，消息进入重试队列
						logger.Info("consume failed, it's return ReconsumeLater.")
					}
				}

			} else if responseCommand.Code == PULL_NOT_FOUND {
			} else if responseCommand.Code == PULL_RETRY_IMMEDIATELY || responseCommand.Code == PULL_OFFSET_MOVED {
				logger.Error("pull message error,code=%d,request=%v", responseCommand.Code, requestHeader)
				var err error
				pullResult, ok := responseCommand.ExtFields.(map[string]interface{})
				if ok {
					if nextBeginOffsetInter, ok := pullResult["nextBeginOffset"]; ok {
						if nextBeginOffsetStr, ok := nextBeginOffsetInter.(string); ok {
							nextBeginOffset, err = strconv.ParseInt(nextBeginOffsetStr, 10, 64)
							if err != nil {
								logger.Error("%s", err)
							}

						}

					}

				}

				//time.Sleep(1 * time.Second)
			} else {
				logger.Error(fmt.Sprintf("pull message error,code=%d,body=%s", responseCommand.Code, string(responseCommand.Body)))
				logger.Error("%#v", pullRequest.messageQueue)
				time.Sleep(1 * time.Second)
			}
		} else {
			logger.Error("responseFuture is nil")
		}

		nextPullRequest := &PullRequest{
			consumerGroup: pullRequest.consumerGroup,
			nextOffset:    nextBeginOffset,
			messageQueue:  pullRequest.messageQueue,
		}

		self.mqClient.pullMessageService.pullRequestQueue <- nextPullRequest
	}

	brokerAddr, _, found := self.mqClient.findBrokerAddressInSubscribe(pullRequest.messageQueue.BrokerName, 0, false)
	if found {
		//currOpaque := atomic.AddInt32(&opaque, 1)
		//remotingCommand := new(RemotingCommand)
		//remotingCommand.Code = PULL_MESSAGE
		//remotingCommand.Opaque = currOpaque
		//remotingCommand.Flag = 0
		//remotingCommand.Language = LANGUAGE
		//remotingCommand.Version = 79

		//remotingCommand.ExtFields = requestHeader

		self.PullKernelImpl(brokerAddr, requestHeader, 1000, CommunicationMode.ASYNC, pullCallback)
	}
}

// PullKernelImpl, include ASNYC AND SNYC communicationMode but not ONEWAY communicationMode
// add by chenjunjie<youaijj@163.com>
func (self *DefaultConsumer) PullKernelImpl(addr string, requestHeader *PullMessageRequestHeader, timeoutMillis int64, communicationMode int, pullCallback InvokeCallback) (*PullResult, error) {
	remotingCommand := createRemotingCommand(PULL_MESSAGE)
	remotingCommand.ExtFields = requestHeader

	switch communicationMode {
	case CommunicationMode.ASYNC:
		err := self.remotingClient.invokeAsync(addr, remotingCommand, 1000, pullCallback)
		return nil, err
	case CommunicationMode.SYNC:
		response, err := self.remotingClient.invokeSync(addr, remotingCommand, 1000)
		if err != nil {
			return nil, err
		}
		return processPullResponse(response)
	}

	return nil, nil
}

// processPullResponse, add by chenjunjie<youaijj@163.com>
func processPullResponse(response *RemotingCommand) (*PullResult, error) {
	pullStatus := PullStatus.NO_NEW_MSG
	switch response.Code {
	case SUCCESS:
		pullStatus = PullStatus.FOUND
		logger.Info("pullStatus is FOUND")
	case PULL_NOT_FOUND:
		pullStatus = PullStatus.NO_NEW_MSG
		logger.Info("pullStatus is PULL_NOT_FOUND")
	case PULL_RETRY_IMMEDIATELY:
		pullStatus = PullStatus.NO_MATCHED_MSG
		logger.Info("pullStatus is PULL_RETRY_IMMEDIATELY")
	case PULL_OFFSET_MOVED:
		pullStatus = PullStatus.OFFSET_ILLEGAL
		logger.Info("pullStatus is PULL_OFFSET_MOVED")
	}

	pullMessageResponseHeader := new(PullMessageResponseHeader)
	map1 := ExtFields2Map(response.ExtFields)
	err := decodeCommandCustomHeader(map1, pullMessageResponseHeader)
	if err != nil {
		return nil, err
	}

	result := new(PullResult)
	if pullMessageResponseHeader == nil {
		return nil, nil
	}

	result.PullStatus = pullStatus
	result.MaxOffset = pullMessageResponseHeader.MinOffset
	result.MinOffset = pullMessageResponseHeader.MinOffset
	result.NextBeginOffset = pullMessageResponseHeader.NextBeginOffset

	if len(response.Body) > 0 {
		msgs := decodeMessage(response.Body)
		if len(msgs) > 0 {
			result.MsgFoundList = msgs

		}
	}

	return result, nil
}

// add by chenjunjie<youaijj@163.com>
// todo
func processPullResult(mq *MessageQueue, pullResult *PullResult, subscriptionData *SubscriptionData) (*PullResult, error) {
	return nil, nil
}

// 注：该方法单独为根据offset查消息服务，根据个人需要可修改或扩展
// add by chenjunjie
func (self *DefaultConsumer) pull(mq *MessageQueue, subExpression string, offset int64, maxNums int32) (*PullResult, error) {
	if mq == nil {
		return nil, fmt.Errorf("mq is null")
	}

	if offset < 0 {
		return nil, fmt.Errorf("offset < 0")
	}

	if maxNums <= 0 {
		return nil, fmt.Errorf("maxNums <= 0")
	}

	subVersion := int64(0)
	sysFlag := FLAG_SUBSCRIPTION
	subscriptionData, err := buildSubscriptionData(self.consumerGroup, mq.Topic, subExpression)
	if err != nil {
		return nil, fmt.Errorf("parse subscription error. %s", err)
	}

	requestHeader := new(PullMessageRequestHeader)
	requestHeader.ConsumerGroup = self.consumerGroup
	requestHeader.Topic = mq.Topic
	requestHeader.QueueId = mq.QueueId
	requestHeader.QueueOffset = offset
	requestHeader.MaxMsgNums = maxNums
	requestHeader.SysFlag = sysFlag
	requestHeader.CommitOffset = 0
	requestHeader.SuspendTimeoutMillis = BrokerSuspendMaxTimeMillis
	requestHeader.Subscription = subscriptionData.SubString
	requestHeader.SubVersion = subVersion

	format := "requestHeader info: ConsumerGroup=%s, topic=%s, queueId=%d, queueOffset=%d, maxNums=%d, sysFlag=%d, commitOffset=%d, brokerSuspendMaxTimeMillis=%d,subExpression=%s, subVersion=%d"
	logger.Info(format, self.consumerGroup, mq.Topic, mq.QueueId, offset, maxNums, sysFlag, 0, BrokerSuspendMaxTimeMillis, subExpression, subVersion)

	if len(self.mqClient.brokerAddrTable) < 1 {
		self.mqClient.updateTopicRouteInfoFromNameServerByTopic(mq.Topic)
	}
	brokerAddr, _, found := self.mqClient.findBrokerAddressInSubscribe(mq.BrokerName, 0, false)
	if found {
		logger.Info("found brokerAddr by by findBrokerAddressInSubscribe")
		pullResult, err := self.PullKernelImpl(brokerAddr, requestHeader, 1000, CommunicationMode.SYNC, nil)
		logger.Info(" ", pullResult, err)
		return pullResult, err
	}

	logger.Info("not found brokerAddr by findBrokerAddressInSubscribe")
	return nil, nil
}

func buildSubscriptionData(consumerGroup, topic, subString string) (*SubscriptionData, error) {
	subscriptionData := new(SubscriptionData)
	subscriptionData.Topic = topic
	subscriptionData.SubString = subString

	if "" == subString || subString == SUB_ALL || len(subString) == 0 {
		subscriptionData.SubString = SUB_ALL
	} else {
		tags := strings.Split(subString, "\\|\\|")
		if tags != nil && len(tags) > 0 {
			for _, tag := range tags {
				trimString := ""
				if len(tag) > 0 {
					trimString = strings.TrimSpace(tag)
				}

				if len(trimString) > 0 {
					subscriptionData.TagsSet = append(subscriptionData.TagsSet, trimString)
					//subscriptionData.CodeSet = append(subscriptionData.CodeSet,hashCodeByTrimeString)

				}
			}
			return nil, fmt.Errorf("subString split error")
		}
	}

	return subscriptionData, nil
}

func (self *DefaultConsumer) updateTopicSubscribeInfo(topic string, info []*MessageQueue) {
	if self.rebalance.subscriptionInner != nil {
		self.rebalance.subscriptionInnerLock.RLock()
		_, ok := self.rebalance.subscriptionInner[topic]
		self.rebalance.subscriptionInnerLock.RUnlock()
		if ok {
			self.rebalance.subscriptionInnerLock.Lock()
			self.rebalance.topicSubscribeInfoTable[topic] = info
			self.rebalance.subscriptionInnerLock.Unlock()
		}
	}
}

// all suscribe(topic-->tags)
func (self *DefaultConsumer) subscriptions() []*SubscriptionData {
	subscriptions := make([]*SubscriptionData, 0)
	for _, subscription := range self.rebalance.subscriptionInner {
		subscriptions = append(subscriptions, subscription)
	}
	return subscriptions
}

func (self *DefaultConsumer) doRebalance() {
	self.rebalance.doRebalance()
}
