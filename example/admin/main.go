package main

import (
	"fmt"
	"git.oschina.net/cloudzone/cloudmq-go-client/cloudmq"
	//"git.oschina.net/cloudzone/smartgo/stgcommon/utils/timeutil"
	"git.oschina.net/cloudzone/cloudcommon-go/logger"
	"git.oschina.net/cloudzone/smartgo/stgcommon/utils/timeutil"
	"strings"
)

var conf *cloudmq.Config

func main() {
	conf = &cloudmq.Config{
	//Nameserver: "10.112.68.191:9876",
	//Nameserver: "10.112.68.190:9876",
	//Nameserver: "10.112.101.110:9876",
	//Nameserver: "10.128.46.90:9876",
	}

	admin, err := cloudmq.NewDefaultAdmin(conf)
	if err != nil {
		panic(err)
	}

	//traceTest(admin)

	//searchClusterInfo(admin)
	//createTopic(admin)
	//searchTopic(admin)
	//queryRetryGroupTopicPrefix(admin)

	//queryConsumerGroupByTopic(admin)
	//queryConsumerConnection(admin)
	//queryConsumerProgress(admin)
	//getAllSubGroupConfig(admin)

	//queryMessageById(admin)  //0A7044BF00002A9F00000003E225C7C2
	//queryMsgByKey(admin)
	//queryMsgByOffset(admin)
	//decodeMessageId(admin)

	//updateBrokerConfig(admin)
	//getBrokerConfig(admin)
	//fetchBrokerRuntimeStats(admin)
	//deleteSubscriptionGroup(admin)
	//updateSubGroup(admin)

	//getProjectGroup(admin)
	//updateProjectGroup(admin)
	//deleteProjectGroup(admin)
	//getKvConfig(admin)
	//updateKvConfig(admin)
	//deleteKvConfig(admin)
	//getKvList(admin)

	//select {}
	//toString(admin)
	admin.Close()
}

func traceTest(admin cloudmq.Admin) {
	msgId := "0A70656D00002A9F0000000C6753CE6A"
	messageExt, err := admin.QueryMessageById(msgId)
	if err != nil {
		logger.Error("query msg messageExt error. ")
		return
	}
	if messageExt == nil {
		logger.Warn("not found messageExt from admin.")
		return
	}

	logger.Info("----------->>>>", messageExt)
	messagetracks, err := admin.MessageTrackDetail(messageExt)
	if err != nil {
		logger.Error("query msg messageExt error.")
		return
	}
	if messagetracks == nil {
		logger.Warn("not found messageTracks from admin.")
		return
	}
}

func getKvList(admin cloudmq.Admin) {
	kvList, err := admin.GetKvList("PROJECT_CONFIG")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("kvList:%#v", kvList)
}

func queryMsgByOffset(admin cloudmq.Admin) {
	topicName := "jcpt-cjj-topicTest1"
	brokerName := "broker-a"
	queueId := int32(0)
	offset := int64(26)
	_, err := admin.QueryMsgByOffset(topicName, brokerName, queueId, offset)
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("pull MSG successed! ")
}

func getBrokerConfig(admin cloudmq.Admin) {
	allBrokerConfig, err := admin.GetBrokerConfig("10.112.68.191:10911")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("allBrokerConfig:%#v", allBrokerConfig)
}

func getAllSubGroupConfig(admin cloudmq.Admin) {
	subConfig, err := admin.GetAllSubGroupConfig("10.112.68.191:10911")
	if err != nil {
		panic(err.Error())
	}

	for _, value := range subConfig {
		fmt.Printf("value.SubGroupName:%v, value.SubscriptionGroupConfig.WhichBrokerWhenConsumeSlowly:%v \n", value.SubGroupName, value.SubscriptionGroupConfig.WhichBrokerWhenConsumeSlowly)
	}

	//fmt.Printf("subConfig:%#v", subConfig)

}

func deleteSubscriptionGroup(admin cloudmq.Admin) {
	has, err := admin.DeleteSubscriptionGroup("", "", "DefaultCluster")
	if err != nil && !has {
		panic(err.Error())
	}
	fmt.Printf("has===%v \n", has)
}

func updateSubGroup(admin cloudmq.Admin) {
	subGroupConfig := &cloudmq.SubscriptionGroupConfig{
		GroupName:                    "fff",
		ConsumeEnable:                true,
		ConsumeFromMinEnable:         false,
		ConsumeBroadcastEnable:       false,
		RetryQueueNums:               1,
		RetryMaxTimes:                16,
		BrokerId:                     0,
		WhichBrokerWhenConsumeSlowly: 1,
	}
	has, err := admin.UpdateSubGroup(subGroupConfig, "", "DEFAULT")
	if err != nil && !has {
		panic(err.Error())
	}
	fmt.Printf("has===%v \n", has)
}

func deleteProjectGroup(admin cloudmq.Admin) {
	has, err := admin.DeleteProjectGroup("10.112.68.185", "PROJECT_CONFIG")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("has===%v \n", has)
}

func updateProjectGroup(admin cloudmq.Admin) {
	has, err := admin.UpdateProjectGroup("10.110.68.185", "CONFIG")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("has===%v \n", has)
}

func getProjectGroup(admin cloudmq.Admin) {
	result, err := admin.GetProjectGroup("10.128.68.185", "PROJECT_CONFIG")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("result:%v \n", result)
}

func deleteKvConfig(admin cloudmq.Admin) {
	result, err := admin.DeleteKvConfig("ORDER_TOPIC_CONFIG123", "jcpt-example-300")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("result===%v \n", result)
}

func updateBrokerConfig(admin cloudmq.Admin) {
	result, err := admin.UpdateBrokerConfig("10.112.68.191:10911", "DefaultCluster", "clientSocketSndBufSize", "131072")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("result===%v \n", result)
}

func updateKvConfig(admin cloudmq.Admin) {
	result, err := admin.UpdateKvConfig("ORDER_TOPIC_CONFIG123", "jcpt-example-300", "broker-master2:8 111")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("result===%v \n", result)
}

func getKvConfig(admin cloudmq.Admin) {
	result, err := admin.GetKVConfig("ORDER", "jcpt-example-300")
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("result===%v \n", result)
}

func queryMsgByKey(admin cloudmq.Admin) {
	topic := "jcpt-qa-simple-800"
	key := "ORDERID_0"
	end := timeutil.CurrentTimeMillis() //(int64)(1488357071)1509956081372
	begin := end - (6 * 60 * 60 * 1000)
	queryResult, err := admin.QueryMsgByKey(topic, key, 32, begin, end) //查找过去6小时 msgKey="cjjTest_Key"的所有消息
	if err != nil {
		panic(err.Error())
	}
	if queryResult == nil || queryResult.MessageList == nil || len(queryResult.MessageList) == 0 {
		logger.Info("---------------------ahkfhaskdfh-------------")
	}

	fmt.Printf("----->>>>> total msg = %d", len(queryResult.MessageList))
	fmt.Printf("\nresult done", queryResult.MessageList[0])
}

func searchTopic(admin cloudmq.Admin) {

	topicData, err := admin.SearchTopic()
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("\nsearch cluser's topic from nameserver[%s], topicList:[%s]\n", conf.Nameserver, strings.Join(topicData.List, ", "))
}

func searchClusterInfo(admin cloudmq.Admin) {

	clusterInfo, err := admin.SearchClusterInfo()
	if err != nil {
		panic(err)
	}

	fmt.Printf("search cluster info from nameserver[%s]: \n", conf.Nameserver)
	for clusterName, brokerNames := range clusterInfo.ClusterAddrTable {
		fmt.Printf("\tclusterName: [%s] brokerNames: %s\n", clusterName, brokerNames)
	}
	for _, brokerData := range clusterInfo.BrokerAddrTable {
		fmt.Printf("\t[%s]: \n", brokerData.BrokerName)
		for brokerId, brokerAddr := range brokerData.BrokerAddrs {
			fmt.Printf("\t\tbrokerId=%d, brokerAddr=%s\n", brokerId, brokerAddr)
		}
	}
}

func createTopic(admin cloudmq.Admin) {

	topic := "jcpt-cjj-topicTest1"
	clusterName := "DefaultCluster"
	ret, err := admin.CreateTopic(topic, clusterName, 8, 8, false)
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("\ncreate or update topic[%s], result: %v\n", topic, ret)
}

func queryMessageById(admin cloudmq.Admin) {
	//msgId := "0A70656D00002A9F0000000840042830"
	msgId := "0A70656D00002A9F0000000C6753CCCC"

	msg, err := admin.QueryMessageById(msgId)
	if err != nil {
		panic(err)
	}
	if msg != nil {
		fmt.Printf("\nquery message by id: msgId=%s|topic=%s|storeTimestamp=%d|bornTimestamp=%d|storeHost=%s|bornHost=%s"+
			"|msgTag=%s|msgKey=%s|sysFlag=%d|storeSize=%d|queueId=%d|queueOffset=%d|commitOffset=%d|Properties=%s|body=%s\n",
			msg.MsgId, msg.Topic, msg.StoreTimestamp, msg.BornTimestamp, msg.StoreHost, msg.BornHost,
			msg.Tag(), msg.Key(), msg.SysFlag, msg.StoreSize, msg.QueueId, msg.QueueOffset, msg.CommitLogOffset, msg.Properties, msg.Body)
	}

	msgTracks, err := admin.MessageTrackDetail(msg)
	if err != nil {
		panic(err)
	}

	fmt.Printf("msg track:\n")
	for i, track := range msgTracks {
		fmt.Printf("%d:\t ConsumerGroup=%s, TrackType=%d, ExceptionDesc=%s\n", i, track.ConsumerGroup, track.TrackType, track.ExceptionDesc)
	}
}

func queryConsumerGroupByTopic(admin cloudmq.Admin) {
	topic := "jcpt-cjj-topicTest1"
	groupNames, err := admin.QueryConsumerGroupByTopic(topic)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nquery consumer group by topic[%s], groupNames:%s\n", topic, groupNames)
}

func queryConsumerConnection(admin cloudmq.Admin) {
	consumerGroupId := "PushConsumer_group"
	consumerConnection, err := admin.QueryConsumerConnection(consumerGroupId)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nquery consumer conection by consumeGroup[%s]: consumeType=%s, MessageModel=%s, consumeFromWhere=%s\n",
		consumerGroupId, consumerConnection.ConsumeType, consumerConnection.MessageModel, consumerConnection.ConsumeFromWhere)
	fmt.Printf("client list(%d)\t|index\t|ClientId\t\t|ClientAddr\t\t|Language\t|Version\n", len(consumerConnection.ConnectionSet))
	for i, conn := range consumerConnection.ConnectionSet {
		fmt.Printf("\t\t|%d\t|%s\t|%s\t|%s\t\t|%d\n", i+1, conn.ClientId, conn.ClientAddr, conn.Language, conn.Version)
	}
	fmt.Printf("Subscribed list\t|Topic\t\t|SubString\t\t|ClassFilterMode\t|TagsSet\t|CodeSet\t|SubVersion\n")
	for key, subData := range consumerConnection.SubscriptionTable {
		fmt.Printf("\t\t|%s\t|%s\t|%v\t|%s\t|%d\t|%d\n", key, subData.SubString, subData.ClassFilterMode, subData.TagsSet, subData.CodeSet, subData.SubVersion)
	}
}

func queryConsumerProgress(admin cloudmq.Admin) {
	consumerGroupId := "PushConsumer_group"
	consumerProgress, err := admin.QueryConsumerProgress(consumerGroupId)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nquery consumer progress by consumeGroup[%s]: tps[%d] diff[%d] \n", consumerGroupId, consumerProgress.Tps, consumerProgress.Diff)
	fmt.Printf("Progress list(%d)\t|Topic\t\t|BrokerName\t\t|QueueId\t|BrokerOffset\t|ConsumerOffset\t|Diff\n", len(consumerProgress.List))
	for _, cg := range consumerProgress.List {
		fmt.Printf("\t|%s\t\t|%s\t\t|%d\t\t|%d\t\t|%d\t\t|%d\n", cg.Topic, cg.BrokerName, cg.QueueId, cg.BrokerOffset, cg.ConsumerOffset, cg.Diff)
	}
}

func fetchBrokerRuntimeStats(admin cloudmq.Admin) {
	brokerAddr := "10.112.68.191:10911"
	brokerRuntimeInfo, err := admin.FetchBrokerRuntimeStats(brokerAddr)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\nquery broker runtime info[%s]: [%#v] \n", brokerAddr, brokerRuntimeInfo)
}

func toString(admin cloudmq.Admin) {
	fmt.Printf("\n %s \n", admin.ToString())
}

func decodeMessageId(admin cloudmq.Admin) {
	msgId := "0A7044BF00002A9F00000003E2262C3A"
	brokerAddr, offset, err := admin.DecodeMessageId(msgId)
	if err != nil {
		panic(err)
	}

	fmt.Printf("\n brokerAddr=%s, offset=%d, msgId=%s \n", brokerAddr, offset, msgId)
}

func queryRetryGroupTopicPrefix(admin cloudmq.Admin) {
	fmt.Printf("retryGroupTopicPrefix = %s \n", admin.QueryRetryGroupTopicPrefix())
}
